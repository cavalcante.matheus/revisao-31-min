#!/bin/bash

existe=0
nao_existe=0

for arquivo in "$@"; do
  if [ -e "$arquivo" ]; then
    existe=$((existe+1))
  else
    nao_existe=$((nao_existe+1))
  fi
done

echo "Quantidade de arquivos existentes: $existe"
echo "Quantidade de arquivos inexistentes: $nao_existe"
