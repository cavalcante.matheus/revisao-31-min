#!/bin/bash

count=0
while [ $count -lt 3 ]
do
    filename=$(($RANDOM%16)).txt
    if [ ! -f "$filename" ]
    then
        touch "$filename"
        count=$(($count+1))
    fi
done
